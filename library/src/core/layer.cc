#include "voxblox/core/layer.h"

#define SERIAL_VERSION "0.0.1"

namespace voxblox
{

    typedef struct layerHeader
    {
        uint32_t num_messages;
        float voxel_size;
        float voxels_per_side;
        std::string type;
        std::string version;
    } layerHeader;

    // Serialization.
    // define how object should be serialized/deserialized
    template <typename S>
    void serialize(S &s, layerHeader &o)
    {
        s.value4b(o.num_messages);
        s.value4b(o.voxel_size);
        s.value4b(o.voxels_per_side);
        s.container1b(o.type, 100); // resizable containers also require maxSize, to make it safe from buffer-overflow attacks
        s.container1b(o.version, 25);
    }

    template <typename VoxelType>
    bool Layer<VoxelType>::loadBlocksFromFile(const std::string &file_path, typename Layer<VoxelType>::BlockMergingStrategy strategy, bool multiple_layer_support)
    {
        if (file_path.empty())
        {
            fprintf(stderr, "Unable to load layer: file path is empty\n");
            return false;
        }

        // Open and check the file
        std::fstream layer_file;
        layer_file.open(file_path, std::fstream::in);
        if (!layer_file.is_open())
        {
            std::cerr << "Could not open file to load layer: " << file_path;
            return false;
        }

        layerHeader read_layer;
        auto state = bitsery::quickDeserialization<bitsery::InputStreamAdapter>(layer_file, read_layer);

        // Check that the data matches
        if (read_layer.version.compare(SERIAL_VERSION))
        {
            fprintf(stderr, "Version number mismatch! This needs to be handled...\n");
            // return false; // commented for now, only 0.0.1 exists
        }
        else if (read_layer.voxel_size != voxel_size_)
        {
            fprintf(stderr, "Voxel size mismatch! Loaded map = %f, Config = %f", read_layer.voxel_size, voxel_size_);
            return false;
        }
        else if (read_layer.voxels_per_side != voxels_per_side_)
        {
            fprintf(stderr, "Voxels per side mismatch! Loaded map = %f, Config = %ld", read_layer.voxels_per_side, voxels_per_side_);
            return false;
        }

        for (int j = 0; j < read_layer.num_messages - 1; j++)
        {
            blockData read_block;

            auto state_ = bitsery::quickDeserialization<bitsery::InputStreamAdapter>(layer_file, read_block);

            voxblox::Point block_origin(read_block.x, read_block.y, read_block.z);

            typename Block<VoxelType>::Ptr block_ptr(new Block<VoxelType>(read_layer.voxels_per_side, read_layer.voxel_size, block_origin));

            block_ptr->set_has_data(read_block.has_data);
            block_ptr->deserializeFromIntegers(read_block.data);
            block_ptr->set_has_data(read_block.data);

            // Mark that this block has been updated.
            block_ptr->updated().set();

            const BlockIndex block_index = getGridIndexFromOriginPoint<BlockIndex>(
                block_origin, block_size_inv_);

            switch (strategy)
            {
            case Layer<VoxelType>::BlockMergingStrategy::kProhibit:
                if (block_map_.count(block_index) == 0u)
                {
                    std::cerr << "Block collision at index: " << block_index << std::endl;
                    exit(-1);
                }
                block_map_[block_index] = block_ptr;
                break;
            // WARNING: kReplace causes occasional segfaults
            case Layer<VoxelType>::BlockMergingStrategy::kReplace:
                try
                {
                    block_map_[block_index] = block_ptr;
                }
                catch (const std::runtime_error &e)
                {
                    block_map_.insert(std::make_pair(block_index, block_ptr));
                }
                break;
            case Layer<VoxelType>::BlockMergingStrategy::kDiscard:
                block_map_.insert(std::make_pair(block_index, block_ptr));
                break;
            case Layer<VoxelType>::BlockMergingStrategy::kMerge:
            {
                typename Layer<VoxelType>::BlockHashMap::iterator it = block_map_.find(block_index);
                if (it == block_map_.end())
                {
                    block_map_[block_index] = block_ptr;
                }
                else
                {
                    it->second->mergeBlock(*block_ptr);
                }
            }
            break;
            default:
                std::cerr << "Unknown BlockMergingStrategy: "
                          << static_cast<int>(strategy);
                return false;
            }
        }
        return true;
    }
    
    template <typename VoxelType>
    bool Layer<VoxelType>::saveToFile(std::string file_path, bool clear_file) const
    {
        constexpr bool kIncludeAllBlocks = true;
        return saveSubsetToFile(file_path, BlockIndexList(), kIncludeAllBlocks, clear_file);
    }

    template <typename VoxelType>
    bool Layer<VoxelType>::saveSubsetToFile(const std::string &file_path,
                                            BlockIndexList blocks_to_include,
                                            bool include_all_blocks,
                                            bool clear_file) const
    {
        if (getType().compare(voxel_types::kNotSerializable) == 0)
        {
            std::cerr << "The voxel type of this layer is not serializable!" << std::endl;
            return false;
        }

        if (file_path.empty())
        {
            std::cerr << "File path is empty";
            return false;
        }
        std::fstream outfile;
        // Will APPEND to the current file in case outputting multiple layers on the
        // same file, depending on the flag.
        std::ios_base::openmode file_flags = std::fstream::out | std::fstream::binary;
        if (!clear_file)
        {
            file_flags |= std::fstream::app | std::fstream::ate;
        }
        else
        {
            file_flags |= std::fstream::trunc;
        }
        outfile.open(file_path, file_flags);
        if (!outfile.is_open())
        {
            std::cerr << "Could not open file for writing: " << file_path << std::endl;
            return false;
        }

        // Only serialize the blocks if there are any.
        // Count the number of blocks that need to be serialized.
        size_t num_blocks_to_write = 0u;
        if ((include_all_blocks && !block_map_.empty()) ||
            !blocks_to_include.empty())
        {
            for (const BlockMapPair &pair : block_map_)
            {
                bool write_block_to_file = include_all_blocks;

                if (!write_block_to_file)
                {
                    BlockIndexList::const_iterator it = std::find(
                        blocks_to_include.begin(), blocks_to_include.end(), pair.first);
                    if (it != blocks_to_include.end())
                    {
                        ++num_blocks_to_write;
                    }
                }
                else
                {
                    ++num_blocks_to_write;
                }
            }
        }
        if (include_all_blocks)
        {
            if (num_blocks_to_write != block_map_.size())
            {
                std::cerr << "number of blocks to write is not equal to block map size" << std::endl;
                exit(-1);
            }
        }
        else
        {
            if (num_blocks_to_write > block_map_.size())
            {
                std::cerr << "number of blocks to write is greater than block map size" << std::endl;
                exit(-1);
            }
            if (num_blocks_to_write > blocks_to_include.size())
            {
                std::cerr << "number of blocks to write is greater than blocks to include size" << std::endl;
                exit(-1);
            }
        }

        // Write out the layer header.
        const uint32_t num_messages = 1u + num_blocks_to_write;
        layerHeader header;
        header.num_messages = num_messages;
        header.voxel_size = voxel_size_;
        header.voxels_per_side = voxels_per_side_;
        header.type = getType();
        header.version = SERIAL_VERSION;

        bitsery::Serializer<bitsery::OutputBufferedStreamAdapter> ser{outfile};
        ser.object(header);
        // flush to writer
        ser.adapter().flush();

        // Serialize blocks.
        saveBlocksToStream(include_all_blocks, blocks_to_include, &outfile);

        outfile.close();
        return true;
    }

    template <typename VoxelType>
    bool Layer<VoxelType>::saveBlocksToStream(bool include_all_blocks,
                                              BlockIndexList blocks_to_include,
                                              std::fstream *outfile_ptr) const
    {
        if (outfile_ptr == nullptr)
        {
            std::cerr << "outfile pointer is null" << std::endl;
            return false;
        }
        for (const BlockMapPair &pair : block_map_)
        {
            bool write_block_to_file = include_all_blocks;
            if (!write_block_to_file)
            {
                BlockIndexList::const_iterator it = std::find(
                    blocks_to_include.begin(), blocks_to_include.end(), pair.first);
                if (it != blocks_to_include.end())
                {
                    write_block_to_file = true;
                }
            }
            if (write_block_to_file)
            {
                pair.second->serializeToIntegers();

                blockData curr_block;

                pair.second->setupSerializationPacket(&curr_block);

                bitsery::Serializer<bitsery::OutputBufferedStreamAdapter> ser{*outfile_ptr};
                ser.object(curr_block);
                // flush to writer
                ser.adapter().flush();
            }
        }
        return true;
    }

    template <typename VoxelType>
    size_t Layer<VoxelType>::getMemorySize() const
    {
        size_t size = 0u;

        // Calculate size of members
        size += sizeof(voxel_size_);
        size += sizeof(voxels_per_side_);
        size += sizeof(block_size_);
        size += sizeof(block_size_inv_);

        // Calculate size of blocks
        size_t num_blocks = getNumberOfAllocatedBlocks();
        if (num_blocks > 0u)
        {
            typename Block<VoxelType>::Ptr block = block_map_.begin()->second;
            size += num_blocks * block->getMemorySize();
        }
        return size;
    }

    template <typename VoxelType>
    std::string Layer<VoxelType>::getType() const
    {
        return getVoxelType<VoxelType>();
    }

} // namespace voxblox