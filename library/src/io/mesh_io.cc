// NOTE: From open_chisel: github.com/personalrobotics/OpenChisel/
// The MIT License (MIT)
// Copyright (c) 2014 Matthew Klingensmith and Ivan Dryanovski
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "voxblox/io/mesh_io.h"

// Define these only in *one* .cc file.
#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

// #define TINYGLTF_NOEXCEPTION // optional. disable exception handling.
#include "voxblox/io/tiny_gltf.h"


namespace voxblox {

bool convertMeshLayerToMesh(const MeshLayer& mesh_layer, Mesh* mesh,
                            const bool connected_mesh,
                            const float vertex_proximity_threshold) {
  if(mesh == nullptr){
    std::cerr << "mesh is null" << std::endl;
    exit(-1);     
  }

  int ret = 0;
  if (connected_mesh) {
    ret = mesh_layer.getConnectedMesh(mesh, vertex_proximity_threshold);
  } else {
    mesh_layer.getMesh(mesh);
  }
  return mesh->size() > 0u && (ret == 0);
}

bool outputMeshLayerAsPly(const std::string& filename,
                          const MeshLayer& mesh_layer) {
  constexpr bool kConnectedMesh = true;
  return outputMeshLayerAsPly(filename, kConnectedMesh, mesh_layer);
}

bool outputMeshLayerAsPly(const std::string& filename,
                          const bool connected_mesh,
                          const MeshLayer& mesh_layer) {
  Mesh combined_mesh(mesh_layer.block_size(), Point::Zero());

  if (!convertMeshLayerToMesh(mesh_layer, &combined_mesh, connected_mesh)) {
    return false;
  }

  bool success = outputMeshAsPly(filename, combined_mesh);
  if (!success) {
    std::cerr << "Saving to PLY failed!";
  }
  return success;
}

bool outputMeshAsPly(const std::string& filename, const Mesh& mesh) {
  std::ofstream stream(filename.c_str());

  if (!stream) {
    return false;
  }

  size_t num_points = mesh.vertices.size();
  stream << "ply" << std::endl;
  stream << "format ascii 1.0" << std::endl;
  stream << "element vertex " << num_points << std::endl;
  stream << "property float x" << std::endl;
  stream << "property float y" << std::endl;
  stream << "property float z" << std::endl;
  if (mesh.hasNormals()) {
    stream << "property float normal_x" << std::endl;
    stream << "property float normal_y" << std::endl;
    stream << "property float normal_z" << std::endl;
  }
  if (mesh.hasColors()) {
    stream << "property uchar red" << std::endl;
    stream << "property uchar green" << std::endl;
    stream << "property uchar blue" << std::endl;
    stream << "property uchar alpha" << std::endl;
  }
  if (mesh.hasTriangles()) {
    stream << "element face " << mesh.indices.size() / 3 << std::endl;
    stream << "property list uchar int vertex_indices"
           << std::endl;  // pcl-1.7(ros::kinetic) breaks ply convention by not
                          // using "vertex_index"
  }
  stream << "end_header" << std::endl;
  size_t vert_idx = 0;
  for (const Point& vert : mesh.vertices) {
    stream << vert(0) << " " << vert(1) << " " << vert(2);

    if (mesh.hasNormals()) {
      const Point& normal = mesh.normals[vert_idx];
      stream << " " << normal.x() << " " << normal.y() << " " << normal.z();
    }
    if (mesh.hasColors()) {
      const Color& color = mesh.colors[vert_idx];
      int r = static_cast<int>(color.r);
      int g = static_cast<int>(color.g);
      int b = static_cast<int>(color.b);
      int a = static_cast<int>(color.a);
      // Uint8 prints as character otherwise. :(
      stream << " " << r << " " << g << " " << b << " " << a;
    }

    stream << std::endl;
    vert_idx++;
  }
  if (mesh.hasTriangles()) {
    for (size_t i = 0; i < mesh.indices.size(); i += 3) {
      stream << "3 ";

      for (int j = 0; j < 3; j++) {
        stream << mesh.indices.at(i + j) << " ";
      }

      stream << std::endl;
    }
  }
  return true;
}


#define MAX_PLY_HEADER_LEN      384
#define MAX_PLY_BYTES_PER_POINT 128

static size_t calc_ply_buffer_size(const Mesh& mesh){
  size_t buf_len = MAX_PLY_HEADER_LEN;
  size_t num_points = mesh.vertices.size();
  buf_len += MAX_PLY_BYTES_PER_POINT * num_points;
  return buf_len;
}

// secondary function to instead save to a generic buffer for sending
int writeMeshAsPly(const MeshLayer& mesh_layer, char** buffer) {
  
  Mesh mesh(mesh_layer.block_size(), Point::Zero());

  if (!convertMeshLayerToMesh(mesh_layer, &mesh, true)) {
    return false;
  }

  size_t buf_size = calc_ply_buffer_size(mesh);

  *buffer = (char*)malloc(buf_size);

  size_t offset = 0;

  size_t num_points = mesh.vertices.size();

  offset += sprintf(*buffer, "ply\n");
  offset += sprintf(*buffer+offset, "format ascii 1.0\n");
  offset += sprintf(*buffer+offset, "element vertex %lu\n", num_points);
  offset += sprintf(*buffer+offset, "property float x\n");
  offset += sprintf(*buffer+offset, "property float y\n");
  offset += sprintf(*buffer+offset, "property float z\n");
  if (mesh.hasNormals()) {
    offset += sprintf(*buffer+offset, "property float normal_x\n");
    offset += sprintf(*buffer+offset, "property float normal_y\n");
    offset += sprintf(*buffer+offset, "property float normal_z\n");
  }
  if (mesh.hasColors()) {
    offset += sprintf(*buffer+offset, "property uchar red\n");
    offset += sprintf(*buffer+offset, "property uchar green\n");
    offset += sprintf(*buffer+offset, "property uchar blue\n");
    offset += sprintf(*buffer+offset, "property uchar alpha\n");
  }
  if (mesh.hasTriangles()) {
    offset += sprintf(*buffer+offset, "element face %lu\n", mesh.indices.size() / 3);
    offset += sprintf(*buffer+offset, "property list uchar int vertex_indices\n");
     // pcl-1.7(ros::kinetic) breaks ply convention by not using "vertex_index"
  }
  offset += sprintf(*buffer+offset, "end_header\n");

  size_t vert_idx = 0;
  for (const Point& vert : mesh.vertices) {
    offset += sprintf(*buffer+offset, "%6.5f %6.5f %6.5f", vert(0), vert(1), vert(2));

    if (mesh.hasNormals()) {
      const Point& normal = mesh.normals[vert_idx];
      offset += sprintf(*buffer+offset, " %6.5f %6.5f %6.5f", normal.x(), normal.y(), normal.z());
    }
    if (mesh.hasColors()) {
      const Color& color = mesh.colors[vert_idx];
      int r = static_cast<int>(color.r);
      int g = static_cast<int>(color.g);
      int b = static_cast<int>(color.b);
      int a = static_cast<int>(color.a);
      // Uint8 prints as character otherwise. :(
      offset += sprintf(*buffer+offset, " %d %d %d %d", r, g, b, a);
    }

    offset += sprintf(*buffer+offset, "\n");
    vert_idx++;
  }
  if (mesh.hasTriangles()) {
    for (size_t i = 0; i < mesh.indices.size(); i += 3) {
      offset += sprintf(*buffer+offset, "3 ");

      for (int j = 0; j < 3; j++) {
        offset += sprintf(*buffer+offset, "%lu ", mesh.indices.at(i + j));
      }

      offset += sprintf(*buffer+offset, "\n");
    }
  }
  return offset;
}


bool outputMeshLayerAsObj(const std::string& filename, const MeshLayer& mesh_layer){
  constexpr bool kConnectedMesh = true;
  return outputMeshLayerAsObj(filename, kConnectedMesh, mesh_layer);
}

/**
 * @param connected_mesh if true vertices will be shared between triangles
 */
bool outputMeshLayerAsObj(const std::string& filename, const bool connected_mesh, const MeshLayer& mesh_layer){
  Mesh combined_mesh(mesh_layer.block_size(), Point::Zero());

  if (!convertMeshLayerToMesh(mesh_layer, &combined_mesh, connected_mesh)) {
    return false;
  }

  bool success = outputMeshAsObj(filename, combined_mesh);
  if (!success) {
    std::cerr << "Saving to PLY failed!";
  }
  return success;
}


bool outputMeshAsObj(const std::string& filename, const Mesh& mesh){
  std::ofstream stream(filename.c_str());

  if (!stream) {
    return false;
  }

  size_t num_points = mesh.vertices.size();
  stream << "#" << std::endl;
  stream << "# object model" << std::endl;
  stream << "#" << std::endl << std::endl;

  for (const Point& vert : mesh.vertices) {
    stream << "v " << vert(0) << " " << vert(1) << " " << vert(2) << std::endl;
  }
  stream << "# " << num_points << " vertices" << std::endl << std::endl;


  if (mesh.hasTriangles()) {
    stream << "g model" << std::endl;
    stream << "s 1" << std::endl;
    for (size_t i = 0; i < mesh.indices.size(); i += 3) {
      stream << "f ";

      for (int j = 0; j < 3; j++) {
        stream << mesh.indices.at(i + j) + 1 << " ";
      }

      stream << std::endl;
    }
    stream << "# " << mesh.indices.size() / 3 << " polygons" << std::endl;
  }
  return true;
}


#define MAX_OBJ_HEADER_LEN      64
#define MAX_OBJ_BYTES_PER_POINT 64

static size_t calc_obj_buffer_size(const Mesh& mesh){
  size_t buf_len = MAX_OBJ_HEADER_LEN;
  size_t num_points = mesh.vertices.size();
  buf_len += MAX_OBJ_BYTES_PER_POINT * num_points;
  return buf_len;
}


// secondary function to instead save to a generic buffer for sending
int writeMeshAsObj(const MeshLayer& mesh_layer, char** buffer){
  Mesh mesh(mesh_layer.block_size(), Point::Zero());

  if (!convertMeshLayerToMesh(mesh_layer, &mesh, true)) {
    return false;
  }

  size_t buf_size = calc_obj_buffer_size(mesh);

  *buffer = (char*)malloc(buf_size);

  size_t offset = 0;

  size_t num_points = mesh.vertices.size();

  offset += sprintf(*buffer, "#\n");
  offset += sprintf(*buffer+offset, "# object model\n");
  offset += sprintf(*buffer+offset, "#\n\n");

  for (const Point& vert : mesh.vertices) {
    offset += sprintf(*buffer+offset, "v %6.5f %6.5f %6.5f\n", vert(0), vert(1), vert(2));
  }
  offset += sprintf(*buffer+offset, "# %lu vertices\n", num_points);

  if (mesh.hasTriangles()) {
    offset += sprintf(*buffer+offset, "g model\n");
    offset += sprintf(*buffer+offset, "s 1\n");

    for (size_t i = 0; i < mesh.indices.size(); i += 3) {
      offset += sprintf(*buffer+offset, "f ");

      for (int j = 0; j < 3; j++) {
        offset += sprintf(*buffer+offset, "%lu ", mesh.indices.at(i + j) + 1);
      }

      offset += sprintf(*buffer+offset, "\n");
    }
    offset += sprintf(*buffer+offset, "# %lu polygons\n", mesh.indices.size() / 3);
  }
  return offset;
}


bool outputMeshLayerAsGltf(const std::string& filename, const MeshLayer& mesh_layer){
  constexpr bool kConnectedMesh = true;
  return outputMeshLayerAsGltf(filename, kConnectedMesh, mesh_layer);
}

/**
 * @param connected_mesh if true vertices will be shared between triangles
 */
bool outputMeshLayerAsGltf(const std::string& filename, const bool connected_mesh, const MeshLayer& mesh_layer){
  Mesh combined_mesh(mesh_layer.block_size(), Point::Zero());

  if (!convertMeshLayerToMesh(mesh_layer, &combined_mesh, connected_mesh)) {
    return false;
  }

  bool success = outputMeshAsGltf(filename, combined_mesh);
  if (!success) {
    std::cerr << "Saving to PLY failed!";
  }
  return success;
}


bool outputMeshAsGltf(const std::string& filename, const Mesh& mesh){
   std::ofstream stream(filename.c_str());

  if (!stream) {
    return false;
  }
  
  // Create a model with a single mesh and save it as a gltf file
  tinygltf::Model m;
  tinygltf::Scene scene;
  tinygltf::Mesh gltf_mesh;
  tinygltf::Primitive primitive;
  tinygltf::Node node;
  tinygltf::Buffer buffer;
  tinygltf::BufferView bufferView1;
  tinygltf::BufferView bufferView2;
  tinygltf::Accessor accessor1;
  tinygltf::Accessor accessor2;
  tinygltf::Accessor accessor3;
  tinygltf::Asset asset;

  // need to resize the buffer to the dimensions of our data
  buffer.data.resize(mesh.getMemorySize());

  size_t offset = 0;
  // indices
  if (mesh.hasTriangles()) {
    for (size_t i = 0; i < mesh.indices.size(); i += 3) {
      for (int j = 0; j < 3; j++) {
        memcpy(buffer.data.data() + offset, &mesh.indices.at(i + j), sizeof(uint16_t));
        offset += sizeof(uint16_t);
      }
    }
  }


  // PADDING - base64 encoding grabge
  int num_to_pad = (mesh.indices.size()*2) % 4;
  if (num_to_pad){
    num_to_pad = 4 - num_to_pad;
  }

  offset += num_to_pad;
  int first_offset = (mesh.indices.size()*2) + num_to_pad;
  offset += 4;
  // end padding


  size_t vert_idx = 0;

  float x_min,y_min,z_min; 
  x_min = y_min = z_min = std::numeric_limits<float>::max();
  float x_max,y_max,z_max;
  x_max = y_max = z_max = std::numeric_limits<float>::min();

  float r_min,g_min,b_min; 
  r_min = g_min = b_min = std::numeric_limits<float>::max();
  float r_max,g_max,b_max;
  r_max = g_max = b_max = std::numeric_limits<float>::min();

  for (const Point& vert : mesh.vertices) {
      if (vert(0) < x_min){
        x_min = vert(0);
      }
      else if (vert(0) > x_max){
        x_max = vert(0);
      }
        if (vert(1) < y_min){
        y_min = vert(1);
      }
      else if (vert(1) > y_max){
        y_max = vert(1);
      }
      if (vert(2) < z_min){
        z_min = vert(2);
      }
      else if (vert(2) > z_max){
        z_max = vert(2);
      }

      memcpy(buffer.data.data() + offset, &vert(0), sizeof(float));
      offset += sizeof(float);
      memcpy(buffer.data.data() + offset, &vert(1), sizeof(float));
      offset += sizeof(float);
      memcpy(buffer.data.data() + offset, &vert(2), sizeof(float));
      offset += sizeof(float);
    if (mesh.hasColors()) {
      const Color& color = mesh.colors[vert_idx];
      
      float r = static_cast<float>(color.r)/255.;
      float g = static_cast<float>(color.g)/255.;
      float b = static_cast<float>(color.b)/255.;

      if (r < r_min){
        r_min = r;
      }
      else if (r > r_max){
        r_max = r;
      }
        if (g < g_min){
        g_min = g;
      }
      else if (g > g_max){
        g_max = g;
      }
      if (b < b_min){
        b_min = b;
      }
      else if (b > b_max){
        b_max = b;
      }

      memcpy(buffer.data.data() + offset, &r, sizeof(float));
      offset += sizeof(float);
      memcpy(buffer.data.data() + offset, &g, sizeof(float));
      offset += sizeof(float);
      memcpy(buffer.data.data() + offset, &b, sizeof(float));
      offset += sizeof(float);
    }
    vert_idx++;
  }

  // "The indices of the vertices (ELEMENT_ARRAY_BUFFER) take up 6 bytes in the
  // start of the buffer.
  bufferView1.buffer = 0;
  bufferView1.byteOffset=0;
  bufferView1.byteLength=first_offset;
  bufferView1.target = TINYGLTF_TARGET_ELEMENT_ARRAY_BUFFER;

  // The vertices take up 36 bytes (n vertices * 3 floating points * 4 bytes)
  // at position 8 in the buffer and are of type ARRAY_BUFFER
  bufferView2.buffer = 0;
  bufferView2.byteOffset=first_offset+4;
  bufferView2.byteLength=mesh.vertices.size() * 24;
  bufferView2.target = TINYGLTF_TARGET_ARRAY_BUFFER;
  bufferView2.byteStride = 24;

  // Describe the layout of bufferView1, the indices of the vertices
  accessor1.bufferView = 0;
  accessor1.byteOffset = 0;
  accessor1.componentType = TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT;
  accessor1.count = mesh.indices.size();
  accessor1.type = TINYGLTF_TYPE_SCALAR;
  accessor1.maxValues.push_back(mesh.vertices.size()-1);
  accessor1.minValues.push_back(0);

  // Describe the layout of bufferView2, the vertices themself
  accessor2.bufferView = 1;
  accessor2.byteOffset = 0;
  accessor2.componentType = TINYGLTF_COMPONENT_TYPE_FLOAT;
  accessor2.count = mesh.vertices.size();
  accessor2.type = TINYGLTF_TYPE_VEC3;
  accessor2.maxValues = {x_max, y_max, z_max};
  accessor2.minValues = {x_min, y_min, z_min};
  accessor2.name = "POSITION";

  // Describe the layout of bufferView2, the colors per vertex
  accessor3.bufferView = 1;
  accessor3.byteOffset = 12;
  accessor3.componentType = TINYGLTF_COMPONENT_TYPE_FLOAT;
  accessor3.count = mesh.vertices.size();
  accessor3.type = TINYGLTF_TYPE_VEC3;
  accessor3.maxValues = {r_max, g_max, b_max};
  accessor3.minValues = {r_min, g_min, b_min};
  accessor3.name = "COLOR_0";

  // Build the mesh primitive and add it to the mesh
  primitive.indices = 0;                 // The index of the accessor for the vertex indices
  primitive.attributes["POSITION"] = 1;  // The index of the accessor for positions
  primitive.attributes["COLOR_0"] = 2;   // The index of the accessor for colors
  primitive.mode = TINYGLTF_MODE_TRIANGLES;
  gltf_mesh.primitives.push_back(primitive);

  // Other tie ups
  node.mesh = 0;
  node.name = "model";
  scene.nodes.push_back(0); // Default scene

  // Define the asset. The version is required
  asset.version = "2.0";
  asset.generator = "tinygltf";

  // Now all that remains is to tie back all the loose objects into the
  // our single model.
  m.scenes.push_back(scene);
  m.meshes.push_back(gltf_mesh);
  m.nodes.push_back(node);
  m.buffers.push_back(buffer);
  m.bufferViews.push_back(bufferView1);
  m.bufferViews.push_back(bufferView2);
  m.accessors.push_back(accessor1);
  m.accessors.push_back(accessor2);
  m.accessors.push_back(accessor3);
  m.asset = asset;

  tinygltf::TinyGLTF gltf;
  gltf.WriteGltfSceneToStream(&m, stream,
                           true, // pretty print
                           false); // write binary


  stream.close();
  return true;
}


// defaults to connected mesh
// returns number of bytes written on success, negative value on error
int writeMeshAsGltf(const MeshLayer& mesh_layer, char** c_buffer){
  std::string str_out;

  Mesh mesh(mesh_layer.block_size(), Point::Zero());

  if (!convertMeshLayerToMesh(mesh_layer, &mesh, true)) {
    return false;
  }
  
  // Create a model with a single mesh and save it as a gltf file
  tinygltf::Model m;
  tinygltf::Scene scene;
  tinygltf::Mesh gltf_mesh;
  tinygltf::Primitive primitive;
  tinygltf::Node node;
  tinygltf::Buffer buffer;
  tinygltf::BufferView bufferView1;
  tinygltf::BufferView bufferView2;
  tinygltf::Accessor accessor1;
  tinygltf::Accessor accessor2;
  tinygltf::Accessor accessor3;
  tinygltf::Asset asset;

  // need to resize the buffer to the dimensions of our data
  buffer.data.resize(mesh.getMemorySize());

  size_t offset = 0;
  // indices
  if (mesh.hasTriangles()) {
    for (size_t i = 0; i < mesh.indices.size(); i += 3) {
      for (int j = 0; j < 3; j++) {
        memcpy(buffer.data.data() + offset, &mesh.indices.at(i + j), sizeof(uint16_t));
        offset += sizeof(uint16_t);
      }
    }
  }

  // PADDING - for base64 garbage
  int num_to_pad = (mesh.indices.size()*2) % 4;
  if (num_to_pad){
    num_to_pad = 4 - num_to_pad;
  }

  offset += num_to_pad;
  int first_offset = (mesh.indices.size()*2) + num_to_pad;
  offset += 4;
  // end padding

  size_t vert_idx = 0;

  float x_min,y_min,z_min; 
  x_min = y_min = z_min = std::numeric_limits<float>::max();
  float x_max,y_max,z_max;
  x_max = y_max = z_max = std::numeric_limits<float>::min();

  float r_min,g_min,b_min; 
  r_min = g_min = b_min = std::numeric_limits<float>::max();
  float r_max,g_max,b_max;
  r_max = g_max = b_max = std::numeric_limits<float>::min();

  for (const Point& vert : mesh.vertices) {
      if (vert(0) < x_min){
        x_min = vert(0);
      }
      else if (vert(0) > x_max){
        x_max = vert(0);
      }
        if (vert(1) < y_min){
        y_min = vert(1);
      }
      else if (vert(1) > y_max){
        y_max = vert(1);
      }
      if (vert(2) < z_min){
        z_min = vert(2);
      }
      else if (vert(2) > z_max){
        z_max = vert(2);
      }

      memcpy(buffer.data.data() + offset, &vert(0), sizeof(float));
      offset += sizeof(float);
      memcpy(buffer.data.data() + offset, &vert(1), sizeof(float));
      offset += sizeof(float);
      memcpy(buffer.data.data() + offset, &vert(2), sizeof(float));
      offset += sizeof(float);
    if (mesh.hasColors()) {
      const Color& color = mesh.colors[vert_idx];
      
      float r = static_cast<float>(color.r)/255.;
      float g = static_cast<float>(color.g)/255.;
      float b = static_cast<float>(color.b)/255.;

      if (r < r_min){
        r_min = r;
      }
      else if (r > r_max){
        r_max = r;
      }
        if (g < g_min){
        g_min = g;
      }
      else if (g > g_max){
        g_max = g;
      }
      if (b < b_min){
        b_min = b;
      }
      else if (b > b_max){
        b_max = b;
      }

      memcpy(buffer.data.data() + offset, &r, sizeof(float));
      offset += sizeof(float);
      memcpy(buffer.data.data() + offset, &g, sizeof(float));
      offset += sizeof(float);
      memcpy(buffer.data.data() + offset, &b, sizeof(float));
      offset += sizeof(float);
    }
    vert_idx++;
  }

  // "The indices of the vertices (ELEMENT_ARRAY_BUFFER) take up 6 bytes in the
  // start of the buffer.
  bufferView1.buffer = 0;
  bufferView1.byteOffset=0;
  bufferView1.byteLength=first_offset;
  bufferView1.target = TINYGLTF_TARGET_ELEMENT_ARRAY_BUFFER;

  // The vertices take up 36 bytes (n vertices * 3 floating points * 4 bytes)
  // at position 8 in the buffer and are of type ARRAY_BUFFER
  bufferView2.buffer = 0;
  bufferView2.byteOffset=first_offset+4;
  bufferView2.byteLength=mesh.vertices.size() * 24;
  bufferView2.target = TINYGLTF_TARGET_ARRAY_BUFFER;
  bufferView2.byteStride = 24;

  // Describe the layout of bufferView1, the indices of the vertices
  accessor1.bufferView = 0;
  accessor1.byteOffset = 0;
  accessor1.componentType = TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT;
  accessor1.count = mesh.indices.size();
  accessor1.type = TINYGLTF_TYPE_SCALAR;
  accessor1.maxValues.push_back(mesh.vertices.size()-1);
  accessor1.minValues.push_back(0);

  // Describe the layout of bufferView2, the vertices themself
  accessor2.bufferView = 1;
  accessor2.byteOffset = 0;
  accessor2.componentType = TINYGLTF_COMPONENT_TYPE_FLOAT;
  accessor2.count = mesh.vertices.size();
  accessor2.type = TINYGLTF_TYPE_VEC3;
  accessor2.maxValues = {x_max, y_max, z_max};
  accessor2.minValues = {x_min, y_min, z_min};
  accessor2.name = "POSITION";

  // Describe the layout of bufferView2, the colors per vertex
  accessor3.bufferView = 1;
  accessor3.byteOffset = 12;
  accessor3.componentType = TINYGLTF_COMPONENT_TYPE_FLOAT;
  accessor3.count = mesh.vertices.size();
  accessor3.type = TINYGLTF_TYPE_VEC3;
  accessor3.maxValues = {r_max, g_max, b_max};
  accessor3.minValues = {r_min, g_min, b_min};
  accessor3.name = "COLOR_0";

  // Build the mesh primitive and add it to the mesh
  primitive.indices = 0;                 // The index of the accessor for the vertex indices
  primitive.attributes["POSITION"] = 1;  // The index of the accessor for positions
  primitive.attributes["COLOR_0"] = 2;   // The index of the accessor for colors
  primitive.mode = TINYGLTF_MODE_TRIANGLES;
  gltf_mesh.primitives.push_back(primitive);

  // Other tie ups
  node.mesh = 0;
  node.name = "model";
  scene.nodes.push_back(0); // Default scene

  // Define the asset. The version is required
  asset.version = "2.0";
  asset.generator = "tinygltf";

  // Now all that remains is to tie back all the loose objects into the
  // our single model.
  m.scenes.push_back(scene);
  m.meshes.push_back(gltf_mesh);
  m.nodes.push_back(node);
  m.buffers.push_back(buffer);
  m.bufferViews.push_back(bufferView1);
  m.bufferViews.push_back(bufferView2);
  m.accessors.push_back(accessor1);
  m.accessors.push_back(accessor2);
  m.accessors.push_back(accessor3);
  m.asset = asset;

  tinygltf::TinyGLTF gltf;
  gltf.WriteGltfSceneToString(&m, str_out,
                           true,   // pretty print
                           false); // write binary (not supported for string writer)

  int bytes_to_write = strlen(str_out.c_str());
  *c_buffer = (char*)malloc(bytes_to_write);
  memcpy(*c_buffer, str_out.c_str(), bytes_to_write);

  return bytes_to_write;
}

}  // namespace voxblox
